package main

import (
	"encoding/json"
	"fmt"
	"os"
)

type building struct {
	Name   string `json:"name"`
	Height int    `json:"height"`
}

func readBuildings(inputFilePath string) ([]building, error) {
	file, err := os.Open(inputFilePath)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	var buildings []building

	err = json.NewDecoder(file).Decode(&buildings)
	if err != nil {
		return nil, err
	}

	if len(buildings) < 3 {
		return nil, fmt.Errorf("er moeten minstens 3 gebouwen in het bestand staan")
	}

	return buildings, nil

}

func compareBuildings(buildings []building) (building, map[string]int) {
	var tallest building
	differences := make(map[string]int)

	for _, b := range buildings {
		if b.Height > tallest.Height {
			tallest = b
		}
	}

	for _, b := range buildings {
		if b != tallest {
			differences[b.Name] = b.Height - tallest.Height
		}
	}

	return tallest, differences
}

func writeComparison(tallest building, differences map[string]int, outputFilePath string) error {
	file, err := os.Create(outputFilePath)
	if err != nil {
		return err
	}
	defer file.Close()

	_, err = fmt.Fprintf(file, "Het hoogste gebouw is %s met een hoogte van %d meter.\n", tallest.Name, tallest.Height)
	if err != nil {
		return err
	}
	_, err = fmt.Fprintf(file, "Verschil in hoogte met andere gebouwen:\n")
	if err != nil {
		return err
	}
	for name, diff := range differences {
		_, err := fmt.Fprintf(file, "%s: %d meter\n", name, diff)
		if err != nil {
			return err
		}
	}
	return nil
}

func main() {
	inputFilePath := "input.json"
	outputFilePath := "output.txt"

	buildings, err := readBuildings(inputFilePath)
	if err != nil {
		fmt.Println("Fout bij het lezen van het bestand:", err)
		os.Exit(1)
	}

	tallest, differences := compareBuildings(buildings)

	err = writeComparison(tallest, differences, outputFilePath)
	if err != nil {
		fmt.Println("Fout bij het schrijven naar het bestand:", err)
		os.Exit(1)
	}
}
